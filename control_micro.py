#!/usr/bin/python3

import signal
import serial

from xbox360controller import Xbox360Controller


class MotorController:

    def __init__(self):
        # threading
        self._thread = None
        self._stopThread = False

        self.isConnected = False
        self.current_step_size = 400

    def move_left(self, steps=200):
        self.sendCommand("mrx " + str(steps))

    def move_right(self, steps=200):
        self.sendCommand("mrx -" + str(steps))

    def move_up(self, steps=200):
        self.sendCommand("mry " + str(steps))

    def move_down(self, steps=200):
        self.sendCommand("mry -" + str(steps))

    def move_table(self, x_steps, y_steps):
        x_steps = x_steps * -1
        self.sendCommand("mr " + str(x_steps) + " " + str(y_steps) + " 0")

    def focus_in(self, steps=200):
        self.sendCommand("mrz " + str(steps))

    def focus_out(self, steps=200):
        self.sendCommand("mrz -" + str(steps))

    def release(self):
        self.sendCommand("release")

    #### serial connection to arduino ###

    def connectArduino(self, device="/dev/ttyUSB0", baudrate=115200):
        if self.isConnected:
            return True

        try:
            self._serial = serial.Serial(device, baudrate)
            self.startThread()
        except:
            return False
        # update open and locked info
        self.isConnected = self._serial.isOpen()
        return self.isConnected


    def disconnectArduino(self):
        self.stopThread()
        if self._serial:
            self._serial.close()
        self.isConnected = False


    def sendCommand(self, cmd):
        """ Sends a command string to the arduino. """
        if self._serial and self._serial.isOpen():
            cmd += ";"
            try:
                self._serial.write(bytearray(cmd, "ASCII"))
            except:
                pass


    def startThread(self):
        if self._thread:
            return
        self._thread = Thread(target=self.runThread)
        self._thread.start()


    def stopThread(self):
        thread = self._thread
        if thread:
            self._stopThread = True
            thread.join()
            self._thread = None
            self._stopThread = False


    def runThread(self):
        while not self._stopThread and self._serial.isOpen():
            try:
                bl = self._serial.readline()
            except:
                self.isConnected = False
                break
            line = bl.decode('unicode_escape')
            print(line)

            ### process events ###
            #TODO

        self._thread = None


motor_controller = MotorController()
motor_controller.connectArduino()


def on_button_a(button):
    steps = motor_controller.current_step_size / 4
    motor_controller.focus_in(steps)


def on_button_b(button):
    steps = motor_controller.current_step_size / 4
    motor_controller.focus_out(steps)


def on_button_x(button):
    motor_controller.current_step_size = 400


def on_button_y(button):
    motor_controller.current_step_size = 2000


def on_button_thumb_r(button):
    motor_controller.current_step_size = 5000
    print("turbo mode")


def on_button_released(button):
    print('Button {0} was released'.format(button.name))


def on_button_trigger_l(button):
    motor_controller.release()


def move_table(axis):
    steps = motor_controller.current_step_size
    motor_controller.move_table(axis.x*steps, axis.y*steps)


def foo(button):
    print("asdf")
    print(button.name)


def main():
    with Xbox360Controller(0, axis_threshold=0.2) as controller:
        print("foo")

        # Button A events
        controller.button_a.when_pressed = on_button_a
        controller.button_b.when_pressed = on_button_b
        controller.button_x.when_pressed = on_button_x
        controller.button_y.when_pressed = on_button_y
        controller.button_thumb_r.when_pressed = on_button_thumb_r
        #controller.button_a.when_released = on_button_released

        controller.hat.when_moved = move_table

        controller.button_trigger_l.when_pressed = on_button_trigger_l

        signal.pause()


if __name__ == "__main__":
    main()

